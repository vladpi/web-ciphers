from .base import BaseCipher


class SubstitutionCipher(BaseCipher):
    def __init__(self, key):
        super(SubstitutionCipher, self).__init__()
        self.key = key.lower()

    def _encrypt_symbol(self, symbol, symbol_idx):
        encrypted_symbol_index = self.alphabet.index(symbol)
        return self.key[encrypted_symbol_index]

    def _decrypt_symbol(self, symbol, symbol_idx):
        decrypted_symbol_index = self.key.index(symbol)
        return self.alphabet[decrypted_symbol_index]
