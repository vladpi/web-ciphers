import abc

from app.const import RUSSIAN_ALPHABET


class BaseCipher(abc.ABC):
    def __init__(self, alphabet=RUSSIAN_ALPHABET):
        self.alphabet = alphabet
        super().__init__()

    def encrypt(self, message):
        encrypted_message = ''

        for symbol_idx, symbol in enumerate(message):
            lower_symbol = symbol.lower()

            if lower_symbol in self.alphabet:
                encrypted_symbol = self._encrypt_symbol(lower_symbol, symbol_idx)
                encrypted_message += encrypted_symbol if symbol.islower() else encrypted_symbol.upper()

            else:
                encrypted_message += symbol

        return encrypted_message

    def decrypt(self, message):
        decrypted_message = ''

        for symbol_idx, symbol in enumerate(message):
            lower_symbol = symbol.lower()

            if lower_symbol in self.alphabet:
                decrypted_symbol = self._decrypt_symbol(lower_symbol, symbol_idx)
                decrypted_message += decrypted_symbol if symbol.islower() else decrypted_symbol.upper()

            else:
                decrypted_message += symbol

        return decrypted_message

    @abc.abstractmethod
    def _encrypt_symbol(self, symbol, symbol_idx):
        raise NotImplementedError

    @abc.abstractmethod
    def _decrypt_symbol(self, symbol, symbol_idx):
        raise NotImplementedError
