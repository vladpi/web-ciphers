from .base import BaseCipher


class CaesarCipher(BaseCipher):
    def __init__(self, rot):
        super(CaesarCipher, self).__init__()
        self.rot = rot

    def _encrypt_symbol(self, symbol, symbol_idx):
        encrypted_symbol_index = (self.alphabet.index(symbol) + self.rot) % len(self.alphabet)
        return self.alphabet[encrypted_symbol_index]

    def _decrypt_symbol(self, symbol, symbol_idx):
        decrypted_symbol_index = (self.alphabet.index(symbol) - self.rot) % len(self.alphabet)
        return self.alphabet[decrypted_symbol_index]
