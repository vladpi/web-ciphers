from .base import BaseCipher


class VigenereCipher(BaseCipher):
    def __init__(self, key):
        super(VigenereCipher, self).__init__()
        self.key = key.lower()

    def _encrypt_symbol(self, symbol, symbol_idx):
        rot = self.alphabet.index(self.key[symbol_idx % len(self.key)])
        encrypted_symbol_index = (self.alphabet.index(symbol) + rot) % len(self.alphabet)
        return self.alphabet[encrypted_symbol_index]

    def _decrypt_symbol(self, symbol, symbol_idx):
        rot = self.alphabet.index(self.key[symbol_idx % len(self.key)])
        decrypted_symbol_index = (self.alphabet.index(symbol) - rot) % len(self.alphabet)
        return self.alphabet[decrypted_symbol_index]
