from app.ciphers.caesar import CaesarCipher


class Rot13Cipher(CaesarCipher):
    def __init__(self):
        ROT = 13
        super(Rot13Cipher, self).__init__(ROT)
