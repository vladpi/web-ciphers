from flask import render_template, request
from flask.views import MethodView

from app.ciphers.substitution import SubstitutionCipher
from app.forms.substitution import SubstitutionForm


class SubstitutionView(MethodView):
    TEMPLATE_NAME = 'ciphers/substitution.html'

    def get(self):
        form = SubstitutionForm()

        context = {'form': form}
        return render_template(self.TEMPLATE_NAME, **context)

    def post(self):
        form = SubstitutionForm(request.form)
        context = {'form': form}

        if form.validate():
            cipher = SubstitutionCipher(form.key.data)
            if form.mode.data == 'encryption':
                encrypted_message = cipher.encrypt(form.text.data)
                context.update(result=encrypted_message)
            else:
                decrypted_message = cipher.decrypt(form.text.data)
                context.update(result=decrypted_message)

        return render_template(self.TEMPLATE_NAME, **context)
