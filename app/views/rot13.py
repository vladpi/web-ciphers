from flask import render_template, request
from flask.views import MethodView

from app.ciphers.rot13 import Rot13Cipher
from app.forms.rot13 import Rot13Form


class Rot13View(MethodView):
    TEMPLATE_NAME = 'ciphers/rot13.html'

    def get(self):
        form = Rot13Form()

        context = {'form': form}
        return render_template(self.TEMPLATE_NAME, **context)

    def post(self):
        form = Rot13Form(request.form)
        context = {'form': form}

        if form.validate():
            cipher = Rot13Cipher()
            if form.mode.data == 'encryption':
                encrypted_message = cipher.encrypt(form.text.data)
                context.update(result=encrypted_message)
            else:
                decrypted_message = cipher.decrypt(form.text.data)
                context.update(result=decrypted_message)

        return render_template(self.TEMPLATE_NAME, **context)
