from flask import render_template, request
from flask.views import MethodView

from app.ciphers.caesar import CaesarCipher
from app.forms.caesar import CaesarForm


class CaesarView(MethodView):
    TEMPLATE_NAME = 'ciphers/caesar.html'

    def get(self):
        form = CaesarForm()

        context = {'form': form}
        return render_template(self.TEMPLATE_NAME, **context)

    def post(self):
        form = CaesarForm(request.form)
        context = {'form': form}

        if form.validate():
            ceasar = CaesarCipher(form.rot.data)
            if form.mode.data == 'encryption':
                encrypted_message = ceasar.encrypt(form.text.data)
                context.update(result=encrypted_message)
            else:
                decrypted_message = ceasar.decrypt(form.text.data)
                context.update(result=decrypted_message)

        return render_template(self.TEMPLATE_NAME, **context)
