from flask.blueprints import Blueprint

from .index import IndexView
from .caesar import CaesarView
from .rot13 import Rot13View
from .vigenere import VigenereView
from .substitution import SubstitutionView

main = Blueprint('main', __name__)

main.add_url_rule('/', view_func=IndexView.as_view('index'))
main.add_url_rule('/caesar/', view_func=CaesarView.as_view('caesar'))
main.add_url_rule('/rot13/', view_func=Rot13View.as_view('rot13'))
main.add_url_rule('/vigenere/', view_func=VigenereView.as_view('vigenere'))
main.add_url_rule('/substitution/', view_func=SubstitutionView.as_view('substitution'))
