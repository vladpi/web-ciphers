from flask import render_template
from flask.views import MethodView


class IndexView(MethodView):
    TEMPLATE_NAME = 'index.html'

    def get(self):
        return render_template(self.TEMPLATE_NAME)
