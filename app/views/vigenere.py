from flask import render_template, request
from flask.views import MethodView

from app.ciphers.vigenere import VigenereCipher
from app.forms.vigemere import VigenereForm


class VigenereView(MethodView):
    TEMPLATE_NAME = 'ciphers/vigenere.html'

    def get(self):
        form = VigenereForm()

        context = {'form': form}
        return render_template(self.TEMPLATE_NAME, **context)

    def post(self):
        form = VigenereForm(request.form)
        context = {'form': form}

        if form.validate():
            cipher = VigenereCipher(form.key.data)
            if form.mode.data == 'encryption':
                encrypted_message = cipher.encrypt(form.text.data)
                context.update(result=encrypted_message)
            else:
                decrypted_message = cipher.decrypt(form.text.data)
                context.update(result=decrypted_message)

        return render_template(self.TEMPLATE_NAME, **context)
