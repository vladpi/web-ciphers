from wtforms.fields.html5 import IntegerField
from wtforms.validators import NumberRange, InputRequired

from .base import BaseCipherForm


class CaesarForm(BaseCipherForm):
    rot = IntegerField('Сдвиг', default=0,
                       validators=[InputRequired(), NumberRange(0, 32, 'Сдвиг должен быть в диапазоне от 0 до 32')])
