from wtforms import StringField
from wtforms.validators import DataRequired

from .base import BaseCipherForm


class VigenereForm(BaseCipherForm):
    key = StringField('Ключ', validators=[DataRequired()])
