from random import shuffle

from wtforms import StringField
from wtforms.validators import DataRequired
from wtforms.validators import ValidationError

from app.const import RUSSIAN_ALPHABET
from .base import BaseCipherForm


def generate_key():
    alphabet = list(RUSSIAN_ALPHABET)
    shuffle(alphabet)
    return ''.join(alphabet)


class SubstitutionForm(BaseCipherForm):
    key = StringField('Ключ', validators=[DataRequired()],
                      default=generate_key)

    def validate_key(self, field):
        if len(set(field.data)) != 33:
            raise ValidationError('Ключ должен содержать 33 различных символа.')
