from flask_wtf import FlaskForm
from wtforms import TextAreaField, RadioField
from wtforms.validators import DataRequired


class BaseCipherForm(FlaskForm):
    text = TextAreaField('Текст', validators=[DataRequired()])
    mode = RadioField('Режим', choices=[('encryption', 'Шифрование'), ('decryption', 'Дешифрование')],
                      default='encryption')
