from flask import Flask

from .config import Config
from .extensions import bootstrap
from .views import main as main_blueprint


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    register_extensions(app)
    register_blueprints(app)

    return app


def register_extensions(app):
    app.register_blueprint(main_blueprint)


def register_blueprints(app):
    bootstrap.init_app(app)
